<?php
include_once 'core/Controller/BaseController.php';


class IndexController extends BaseController
{
    public $tree = [];
    public $treeData = [];
    public $treeParents = [];

    public function load($arg = [])
    {
        $this->loadTreeInfo();

        $data = [];
        $data['content'] = $this->showTreeChilds(0);
        $this->assignChildInfoData($data);

        $this->view('DefaultView', $data);
    }

     protected function showTreeChilds($parent)
    {
        $childs = $this->getTreeChilds($parent);
        $html = '<ul id="childs-' . $parent . '" class="childs" style="padding-left:' . ($parent * 10 + 10) . 'px" >';
        foreach ($childs as $child) {
            $has_childs = $this->getChilds($child->id);
            $cnt = isset($this->treeParents[$child->id]) ? $this->treeParents[$child->id] : 0;

            $html .= "<li>";
            $html .= '['.$child->id . '] ' .'<a href="?info=' . $child->id . '">' . $child->name . '</a>';
            $html .= $cnt ? ' (<a href="javascript:void(0);" onclick="APP.toggle(this)" id="'.$child->id.'">+</a>)': '';
            $html .= $has_childs ? $this->showTreeChilds($child->id) : '';
            $html .= "</li>";
        }
        $html .= "</ul>";

        return $html;
    }


}