<?php require_once('Header.php') ?>

<table width="70%" border="1" align="center" cellpadding="4" cellspacing="0">
    <caption>[<a href="/admin">Log in</a>]</caption>
    <tr>
        <td width="60%" rowspan="2"><?php echo $data['content'] ?></td>
        <td width="40%">
            <?php if(isset($data['child'])): ?>
                    <p>Parent: <?php echo $data['child']['parent_name'] ?></p>
                    <p>Name: <?php echo $data['child']['name'] ?></p>
                    <p>Description:<br/><?php echo $data['child']['descr'] ?></p>
            <?php endif; ?>

        </td>
    </tr>
</table>

<?php require_once('Footer.php') ?>