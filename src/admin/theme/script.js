var APP = {
    toggle: function (link) {
        var id = link.getAttribute('id');
        var id_childs = 'childs-' + id;
        this.togglebyId(id_childs);

        var link_toggle = document.getElementById(id);
        if (link_toggle) {
            var icon = link_toggle.innerText;
            var new_icon = (icon == '+') ? '-' : '+';
            link_toggle.innerText = new_icon;
        }

    },
    togglebyId: function (id) {
        var x = document.getElementById(id);
        if (x) {
            if (!x.style.display || x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    }
}