<?php require_once ('Header.php') ?>

<table width="70%" border="1" align="center" cellpadding="4" cellspacing="0">
    <caption>Admin [<a href="/admin/user/logout">Logout</a>]</caption>
    <tr>
        <td width="60%" rowspan="2"><?php echo $data['content'] ?></td>
        <td width="40%">
            <form action="/admin/index/insert" method="POST">
                <p>Parent:
                    <input type="text" name="parent" value="">
                </p>
                <p>Name: <input type="text" required="true" name="name" value=""></p>
                <p>Description: <br/><textarea name="descr" rows="3"></textarea></p>
                <p><input type="submit" value="Add" name="submit"></p>
            </form>


        </td>
    </tr>
    <tr>
        <td style="background-color: khaki">
            <?php if(isset($data['child'])): ?>
                <form action="/admin/index/update" method="POST">
                    <p>Parent: <?php echo $data['child']['parent_name'] ?>
                        <input type="text" name="parent" value="<?php echo $data['child']['parent'] ?>">
                    </p>
                    <p>Name: <input type="text" required="true" name="name" value="<?php echo $data['child']['name'] ?>"></p>
                    <p>Description:<br/><textarea name="descr" rows="3"><?php echo $data['child']['descr'] ?></textarea></p>
                    <p><input type="submit" value="Update" name="submit"></p>
                    <p><label><input type="checkbox" value="1" name="remove"> Remove</label></p>

                    <input type="hidden"  name="id" value="<?php echo $data['child']['id'] ?>">
                </form>
            <?php endif; ?>

        </td>
    </tr>
</table>

<?php require_once ('Footer.php') ?>