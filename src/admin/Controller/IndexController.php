<?php
include_once 'core/Controller/BaseController.php';


class IndexController extends BaseController
{
    public $tree = [];
    public $treeParents = [];

    public function login($arg = [])
    {
        $this->view('FormLoginView', $arg);
    }


    public function insert($arg = [])
    {
        if (isset($_POST['submit']) && !empty($_POST['name'])) {
            $name = $_POST['name'];
            $parent = (int)$_POST['parent'];
            $descr = stripslashes(trim($_POST['descr']));

            $sql = "insert into tree set parent = $parent,name='$name', descr='$descr'";
            mysql_query($sql, $this->dbconn);

            header("location: /admin");
        }
    }

    public function update($arg = [])
    {
      //  print_r($_POST);
        if (isset($_POST['submit']) && !empty($_POST['id'])) {
            $id = $_POST['id'];

            if(!empty($_POST['remove'])){
                $this->removeChild($id);
                header("location: /admin");
            }
            else {
                $name = $_POST['name'];
                $parent = (int) $_POST['parent'];
                $descr = stripslashes(trim($_POST['descr']));

                $sql = "update tree set parent = $parent, name='$name', descr='$descr' where id = $id";
                mysql_query($sql, $this->dbconn);

                header("location: /admin/?info=" . $id);
            }
        }
    }

    /**
     * Recursively remove tree item and his childs.
     */
    private function removeChild($id){
        if ($childs = $this->getChilds($id)) {
           foreach($childs as $child){
               $this->removeChild($child->id);
           }
        }
        else {
            //Remove from db.
            $sql = "delete from tree where id = $id LIMIT 1";
            mysql_query($sql, $this->dbconn);
            // Remove from array.
            unset($this->treeParents[$id]);
        }
    }

    public function load($arg = [])
    {
        $this->loadTreeInfo();

        $data = [];
        $data['content'] = $this->showTreeChilds(0);
        $this->assignChildInfoData($data);

        $this->view('DefaultView', $data);
    }

    protected function showTreeChilds($parent)
    {
        $childs = $this->getTreeChilds($parent);
        $html = '<ul id="childs-' . $parent . '" class="childs" style="padding-left:' . ($parent * 6 + 10) . 'px" >';
        foreach ($childs as $child) {
            $has_childs = $this->getChilds($child->id);
            $cnt = isset($this->treeParents[$child->id]) ? $this->treeParents[$child->id] : 0;

            $html .= "<li>";
            $html .= '['.$child->id . '] ' .'<a href="?info=' . $child->id . '">' . $child->name . '</a>';
           // $html .= $cnt ? ' (<a href="javascript:void(0);" onclick="APP.toggle(this)" id="'.$child->id.'">+</a>)': '';
            $html .= $has_childs ? $this->showTreeChilds($child->id) : '';
            $html .= "</li>";
        }
        $html .= "</ul>";

        return $html;
    }

}