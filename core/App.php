<?php

class App
{
    const dbconfig = array(
        'host' => 'database',
        'user' => 'lamp',
        'pass' => 'lamp',
        'database' => 'lamp'
    );

    public $dbconn = false;
    public $root = '';
    public $area = 'public';

    public function __construct(){
        global $_ROOT;
        $this->root = $_ROOT;

        session_start ();
    }

    private function accessPage(){
        return isset($_SESSION['SID']);
    }

    public function login(){

        if (isset($_POST['submit'])) {
            if (empty($_POST['username']) || empty($_POST['password'])) {
                $error = "Username or Password is invalid";
            } else {
// Define $username and $password
                $username = $_POST['username'];
                $password = $_POST['password'];

// SQL query to fetch information of registerd users and finds user match.
                $sql = "select * from users where password='". md5($password)."' AND username='$username' LIMIT 1";
                $query = mysql_query($sql, $this->dbconn);
                $row = mysql_fetch_object($query);

                if($row){
                    $_SESSION['SID'] = md5(implode('-', array($row->id, mt_rand())));
                    return true;
                }
                return false;
            }
        }
    }

    public function getPage(){
       $url_arg =  isset($_GET['q'])? explode('/', $_GET['q']) : [];
       $Area = $url_arg? $url_arg[0] : 'public';
       $Action = 'load';
       $Controller = 'index';
       $this->area = 'public';

       // Admin area changes
       if($Area == 'admin'){
           $this->area = 'admin';
           array_shift($url_arg);
           if(isset($url_arg[0])) {
               $Controller = $url_arg[0];
               array_shift($url_arg);

               if (isset($url_arg[0])) {
                   $Action = $url_arg[0];
                   array_shift($url_arg);
               }
           }

           if (!$this->accessPage()){
               $Controller = 'user';
               $Action = 'login';
           }

//         if(!empty($url)) {
//            $Controller = $url[0] . 'Controller';
//            $arg = array_shift($url);
//         }
       }

       $Controller = ucfirst($Controller . 'Controller');
       $file = 'src/' . $this->area .'/Controller/'. $Controller . '.php';
       if (file_exists($file)){
           include_once $file;

           $conn = new $Controller();
           $conn->{$Action}($url_arg);
       }
    }

}

$App = new App();