<?php

class BaseController {
    public $app;
    public $db;
    public $root;
    public $request_uri;

    function __construct(){
        global $App;

        $this->app = $App;
        $this->dbconn = $App->dbconn;
        $this->root = $this->app->root . '/src/' . $this->app->area;
        $this->request_uri = parse_url($_SERVER['REQUEST_URI']);
    }

    public function load($arg = []){
        $html = 'Default page html';
        return $html;
    }

    public function view($view_name, $data = []) {
        ob_start();
        require  $this->root .'/Views/'.$view_name.'.php';
        echo ob_get_clean();
    }

    protected function assignChildInfoData(&$data)
    {
        $var = [];
        if (!empty($this->request_uri['query'])) {
            parse_str($this->request_uri['query'], $var);

            if (isset($var['info'])) {
                $id = (int)$var['info'];
                $child = (array)$this->tree[$id];
                $parent = $child['parent']>0? $this->tree[$child['parent']] : '';
                $child['parent_name'] = $parent? $parent->name : '';

                $data['child'] = $child;
            }
        }
    }

    protected function loadTreeInfo()
    {
        $sql = 'SELECT * FROM tree order by parent';
        $result = mysql_query($sql, $this->dbconn);
        while ($row = mysql_fetch_object($result)) {
            $this->tree[$row->id] = $row;
            $cnt = isset($this->treeParents[$row->parent]) ? $this->treeParents[$row->parent] : 0;
            $this->treeParents[$row->parent] = $cnt + 1;
        }
    }

    protected function showTreeChilds($parent)
    {
        $childs = $this->getTreeChilds($parent);
        $html = '<ul id="childs-' . $parent . '" class="childs" style="padding-left:' . ($parent * 6 + 10) . 'px" >';
        foreach ($childs as $child) {
            $has_childs = $this->getChilds($child->id);
            $cnt = isset($this->treeParents[$child->id]) ? $this->treeParents[$child->id] : 0;

            $html .= "<li>";
            $html .= '['.$child->id . '] ' .'<a href="?info=' . $child->id . '">' . $child->name . '</a>';
            $html .= $cnt ? ' (<a href="javascript:void(0);" onclick="APP.toggle(this)" id="'.$child->id.'">+</a>)': '';
            $html .= $has_childs ? $this->showTreeChilds($child->id) : '';
            $html .= "</li>";
        }
        $html .= "</ul>";

        return $html;
    }

    protected  function getChilds($id){
        return isset($this->treeParents[$id])? $this->treeParents[$id] : [];
    }

    protected function getTreeChilds($parent)
    {
        $childs = [];
        foreach ($this->tree as $child) {
            if ($child->parent == $parent) {
                $childs[] = $child;
            }
        }
        return $childs;
    }

}