<?php
global $App;
$conn = App::dbconfig;

if (!$App->dbconn = mysql_connect($conn['host'], $conn['user'], $conn['pass'])) {
    echo 'Could not connect to mysql';
    exit;
}

if (!mysql_select_db($conn['database'], $App->dbconn)) {
    echo 'Could not select database';
    exit;
}
